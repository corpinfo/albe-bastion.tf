# Key_Pair
resource "aws_key_pair" "web_server" {
  key_name   = "web_server_key_pair_${terraform.workspace}"
  public_key = file("~/.ssh/id_rsa.pub")
}

# ASG CPU High Scaling Policy
resource "aws_autoscaling_policy" "asg_cpu_high" {
  name                   = "${terraform.workspace}-${var.web-server}-ASG-CPUHigh-SP"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web_server_ASG.name
}

# ASG CPU Low Scaling Policy
resource "aws_autoscaling_policy" "asg_cpu_low" {
  name                   = "${terraform.workspace}-${var.web-server}-CPULow-SP"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web_server_ASG.name
}
# CPU High Alarm for CPU High Scaling Policy
resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  alarm_name          = "${terraform.workspace}-${var.web-server}-CPUHigh-Alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = var.CPUHighPolicy

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web_server_ASG.name
  }

  alarm_description = "This metric monitors ec2 ASG high cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.asg_cpu_high.arn]
}

# CPU Low Alarm for CPU Low Scaling Policy
resource "aws_cloudwatch_metric_alarm" "cpu_low" {
  alarm_name          = "${terraform.workspace}-${var.web-server}-CPULow-Alarm"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "300"
  statistic           = "Average"
  threshold           = var.CPULowPolicy

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web_server_ASG.name
  }

  alarm_description = "This metric monitors ec2 ASG low cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.asg_cpu_low.arn]
}
# Autoscaling-Group
resource "aws_autoscaling_group" "web_server_ASG" {
  name                      = "${terraform.workspace}-${var.web-server}-ASG"
  desired_capacity          = var.desired_capacity
  max_size                  = var.max_size
  min_size                  = var.min_size
  health_check_grace_period = var.ASG_HC_GracePeriod
  force_delete              = true
  vpc_zone_identifier       = data.terraform_remote_state.vpc.outputs.priv-snet
  target_group_arns         = [aws_lb_target_group.nlb-target.arn]


  #Launch the latest template
  launch_template {
    id      = aws_launch_template.web_server_launch_config.id
    version = aws_launch_template.web_server_launch_config.latest_version
  }

}

# Launch Template
resource "aws_launch_template" "web_server_launch_config" {
  name                                 = "${terraform.workspace}-${var.web-server}-LT"
  update_default_version               = true
  image_id                             = var.instance_ami
  instance_initiated_shutdown_behavior = "terminate"
  instance_type                        = var.ec2_instance_type
  key_name                             = aws_key_pair.web_server.id
  vpc_security_group_ids               = [aws_security_group.instance-sg.id]
  user_data                            = filebase64("${path.module}/userdata.sh")
  iam_instance_profile {
    name = aws_iam_instance_profile.instance_profile.name
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "${terraform.workspace}-${var.web-server}"
    }
  }
}