# VPC Remote State
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.bucket
    key    = "env:/common/vpc.tfstate"
    region = var.region
  }
}

# Dev Remote State
data "terraform_remote_state" "bastion-host" {
  backend = "s3"
  config = {
    bucket = var.bucket
    key    = "env:/dev/bastion-host.tf"
    region = var.region
  }
}
