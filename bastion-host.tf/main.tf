# Network LoadBalancer
resource "aws_lb" "nlb" {
  name               = "${terraform.workspace}-${var.bastion-hostName}-my-nlb"
  internal           = false
  load_balancer_type = "network"
  subnets            = data.terraform_remote_state.vpc.outputs.pub-snet

  tags = {
    Name = "dev"
  }
}


# Network Loadbalancer target-group
resource "aws_lb_target_group" "nlb-target" {
  name     = "${terraform.workspace}-${var.bastion-hostName}-nlb-tg"
  port     = 22
  protocol = "TCP"
  vpc_id   = data.terraform_remote_state.vpc.outputs.vpc_id
}

# Network Loadbalancer listerner
resource "aws_lb_listener" "nlb-listerner" {
  load_balancer_arn = aws_lb.nlb.arn
  port              = "22"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb-target.arn
  }
}


# Key_Pair
resource "aws_key_pair" "bastion_host_dev-key" {
  key_name   = "key_pair_${terraform.workspace}"
  public_key = file("~/.ssh/id_rsa.pub")
}


# Autoscaling-Group
resource "aws_autoscaling_group" "ASG" {
  name                = "${terraform.workspace}-${var.bastion-hostName}-ASG"
  desired_capacity    = var.desired_capacity
  max_size            = var.max_size
  min_size            = var.min_size
  force_delete        = true
  vpc_zone_identifier = data.terraform_remote_state.vpc.outputs.pub-snet
  target_group_arns   = [aws_lb_target_group.nlb-target.arn]


  #Launch the latest template
  launch_template {
    id      = aws_launch_template.launch_config.id
    version = "$Latest"
  }

}

// data "templatefile" "config" {
//   template = templatefile("config.tmpl")
//   // template = file("${path.module}/config.tmpl")
//   vars = {
//     bucket = data.terraform_remote_state.dev.outputs.username
//     bucket = data.terraform_remote_state.dev.outputs.ssh_key
//   }
// }

# Launch Template
resource "aws_launch_template" "launch_config" {
  name                                 = "${terraform.workspace}-${var.bastion-hostName}-LT"
  update_default_version               = true
  image_id                             = var.instance_ami
  instance_initiated_shutdown_behavior = "terminate"
  instance_type                        = var.ec2_instance_type
  key_name                             = aws_key_pair.bastion_host_dev-key.id
  vpc_security_group_ids               = [aws_security_group.bastion-sg.id]
  // user_data                            = filebase64(templatefile("${path.module}user_data.tftpl", { department = var.user_department, name = var.user_name }))
  // user_data                            = filebase64(templatefile("config.tmpl", { name = var.username1 } ))
  // user_data                            = templatefile.config.rendered
  // user_data                            = templatefile("config.tftpl", { usernames = data.terraform_remote_state.dev.outputs.username, ssh_key = data.terraform_remote_state.dev.outputs.ssh_key })
  user_data                            = filebase64("${path.module}/userdata.sh")
  // user_data                            = filebase64(templatefile("${path.module}/userdata.sh" ))
  iam_instance_profile {
    name = aws_iam_instance_profile.instance_profile.name
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "${terraform.workspace}-${var.bastion-hostName}"
    }
  }
}