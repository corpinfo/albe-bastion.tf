output "NLB_Name" {
  value = aws_lb.nlb.name

}
output "NLB_dns" {
  value = aws_lb.nlb.dns_name

}

output "template" {
  value = templatefile("config.tmpl",
    {
      "usernames" = local.usernames
      "ssh_key"   = local.ssh_key
  })
}

// variable "example-list" {
//   default = ["one","two","three"]
// }

// # Returns "one-two-three"
// output "join-example" {
//   value = "${join("-",var.example-list)}"
// }