#!/bin/bash

for user in $( cat ./user-list1.txt ); do
    
    mkdir -p /home/$user/.ssh
    cat /path/to/pubkey >> /home/$user/.ssh/authorized_keys
    chmod 777 /home/$user/.ssh
    chmod 640 /home/$user/.ssh/authorized_keys
    chown -R $user.$user /home/$user/.ssh
    
done