deploy-base:
	DEPLOY_ENVIRONMENT=common runway deploy --tag base
deploy-vpc:
	DEPLOY_ENVIRONMENT=common runway deploy --tag vpc
deploy-common:
	DEPLOY_ENVIRONMENT=common runway deploy 

deploy-dev:
	DEPLOY_ENVIRONMENT=dev runway deploy 

destroy-common:
	DEPLOY_ENVIRONMENT=common runway destroy 
destroy-dev:
	DEPLOY_ENVIRONMENT=dev runway destroy 